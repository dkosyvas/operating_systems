#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "tree.h"
#include "proc-common.h"

void fork_procs(struct tree_node *root)
{
	int i=0,status;
	pid_t *pid;

	/*
	 * Start
	 */
	printf("PID = %ld, name %s, starting...\n",(long)getpid(), root->name);
	change_pname(root->name);

	pid=malloc(sizeof(pid_t)*root->nr_children);
	if (pid==NULL) { exit(3);} 	// fail to allocate memory  
	
	//8ewroume oti ka8e process dhmiourgei ta subprocesses xwris omws na perimenei ksexwrista gia ka8e ena na termatisei ,alla perimenei na termatisoun ola .
	
	while(i<root->nr_children){
		pid[i]=fork();	
		if (pid[i]<0) {
			 perror("fork");
			 exit(EXIT_FAILURE); 
		}
		if (pid[i]==0){ //create processes with BFS traversal
			 	//child 
			fork_procs(root-> children + i); // children[i] doesn run for some reason
		}
		i++;
	}
	/* This function makes sure a number of children processes
	 * have raised SIGSTOP, by using waitpid() with the WUNTRACED flag.
 	* This will NOT work if children use pause() to wait for SIGCONT.
	*/
	
	if (root->nr_children !=0){
		wait_for_ready_children(root->nr_children);
	}
	

	raise(SIGSTOP); //put current process to pause by sending SIGSTOP to the pid
	//RAISE_LABEL:
		
	printf("PID = %ld, name = %s is awake\n",(long)getpid(), root->name);

	for(i=0;i<root->nr_children;i++){
		kill(pid[i],SIGCONT); //continue stopped process (child) ( jmp  RAISE_LABEL )
		pid[i]=wait(&status); //  suspends execution until one of its children change state 
		explain_wait_status(pid[i],status);
	}	
	free(pid);
	exit(0);
}

/*
 * The initial process forks the root of the process tree,
 * waits for the process tree to be completely created,
 * then takes a photo of it using show_pstree().
 *
 * How to wait for the process tree to be ready?
 * In ask2-{fork, tree}:
 *      wait for a few seconds, hope for the best.
 * In ask2-signals:
 *      use wait_for_ready_children() to wait until
 *      the first process raises SIGSTOP.
 */

int main(int argc, char *argv[])
{
	pid_t pid;
	int status;
	struct tree_node *root;

	if (argc < 2){
		fprintf(stderr, "Usage: %s <tree_file>\n", argv[0]);
		exit(1);
	}

	/* Read tree into memory */
	root = get_tree_from_file(argv[1]);

	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs(root);
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	wait_for_ready_children(1);

	/* for ask2-{fork, tree} */
	/* sleep(SLEEP_TREE_SEC); */

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* continue execution of father process */
	kill(pid, SIGCONT);

	/* Wait for the root of the process tree to terminate */
	wait(&status);
	explain_wait_status(pid, status);

	return 0;
}
