[//]: <> (convert to pdf with: pandoc --pdf-engine=xelatex --highlight-style tango --reference-links  anafora.md -o anafora.pdf -V mainfont="DejaVu Serif" -V monofont="DejaVu Sans Mono" -V geometry:"top=3cm, bottom=2.3cm, left=2.5cm, right=2.5cm" )

---
header-includes:
 - \usepackage{fvextra}
 - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
 
 - \hypersetup{colorlinks=false,
            allbordercolors={0 0 0.5},
            pdfborderstyle={/S/U/W 1}}

            
 - \usepackage{float}
 - \floatplacement{figure}{H}
---

![ ](pyrforos.png )


# Εθνικό Μετσόβιο Πολυτεχνείο 
## Σχολή Ηλεκτρολόγων Μηχανικών και Μηχανικών Υπολογιστών 
## Λειτουργικά  Συστήματα , 2020



## 1η Εργαστηριακή Άσκηση
#### Ομάδα:B24

Δημήτριος Κόσυβας :03114828

 




## Περιεχόμενα:

1. [Ζήτημα 1.1 Δημιουργία Δέδομένου Δέντρου Διεργασιών ](#label1)
2. [Ζήτημα 1.2 Δημιουργία Αυθαίρετου Δέντρου Διεργασιών ](#label2)
3. [Ζήτημα 1.3 Αποστολή και Χειρισμός Σημάτων](#label3)
4. [Ζήτημα 1.4 Παράλληλος Υπολογισμός Αριθμητικής Έκφρασης](#label4)

## Ζήτημα 1.1 Δημιουργία Δέδομένου Δέντρου Διεργασιών{#label1}

### aks2-fork.c


```c
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "proc-common.h"

#define SLEEP_PROC_SEC  5
#define SLEEP_TREE_SEC  3

/*
 * Create this process tree:
 * A-+-B---D
 *   `-C
 */
void fork_procs(void)
{
	
	int status;
	pid_t b,c,d;
	
	printf("PID= %ld ,name %s ,starting ...\n",(long)getpid(),"A");

	change_pname("A");
	
	b=fork(); 
	if (b<0) {
		perror("main:fork");
		exit(EXIT_FAILURE);
	}
	if (b==0) { // child B
		printf("PID= %ld ,name %s ,starting ...\n",(long)getpid(),"B");
		change_pname("B");
		d=fork();
		if (d<0) { perror("fork"); exit(EXIT_FAILURE); }
		if (d==0){
			printf("PID= %ld ,name %s ,starting ...\n",(long)getpid(),"D");
			change_pname("D");
			
			printf("D: Sleeping...\n");
			sleep(SLEEP_PROC_SEC);
				
			printf("D: Exiting...\n");
			exit(13);
		}
		printf("B:Waiting for child  %ld ...\n",(long) d) ; // B waits for D  to terminate 
		wait (&status) ; // wait for process to change state (scenarios the child terminated; the child was stopped by a signal; or the child was resumed by a signal)
		explain_wait_status(d,status);
		printf("B: Exiting ...\n");
		exit(19);
		}
	c=fork();
	if (c<0) { perror("fork"); exit(EXIT_FAILURE); }
	
	if (c==0){
		printf("PID= %ld ,name %s ,starting ...\n",(long)getpid(),"C");
                change_pname("C");

                printf("C: Sleeping...\n");
                sleep(SLEEP_PROC_SEC+3);

                printf("C: Exiting...\n");
                exit(17);
	}
	printf("A  waiting for children \n ");
	wait(&status);
	explain_wait_status(b,status);

	wait(&status);
        explain_wait_status(c,status);	

	printf("A: Exiting...\n");
	exit(16);
}

/*
 * The initial process forks the root of the process tree,
 * waits for the process tree to be completely created,
 * then takes a photo of it using show_pstree().
 *
 * How to wait for the process tree to be ready?
 * In ask2-{fork, tree}:
 *      wait for a few seconds, hope for the best.
 * In ask2-signals:
 *      use wait_for_ready_children() to wait until
 *      the first process raises SIGSTOP.
 */
int main(void)
{
	pid_t pid;
	int status;

	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs();
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	/* wait_for_ready_children(1); */

	/* for ask2-{fork, tree} */
	sleep(SLEEP_TREE_SEC);

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* for ask2-signals */
	/* kill(pid, SIGCONT); */

	/* Wait for the root of the process tree to terminate */
	pid = wait(&status);
	explain_wait_status(pid, status);

	return 0;
}

```


### Ερωτήσεις

### 1)

### 2)


### 3)


## Ζήτημα 1.2 Δημιουργία Αυθαίρετου Δέντρου Διεργασιών{#label2}
### aks2-tree.c

```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "tree.h"
#include "proc-common.h"

#define SLEEP_PROC_SEC 6
#define SLEEP_TREE_SEC 2 


void createProcTree( struct tree_node *node)
{
	int i=0,status;
	pid_t *pid;
	pid = (pid_t *) malloc (sizeof(pid_t) * node->nr_children );
	printf("%s: hi my pid is %ld \n",node->name, (long) getpid());
	change_pname(node->name); //name change
	
	while(i < node->nr_children ) {
		pid[i]=fork();
		if ( pid[i] < 0) { exit(EXIT_FAILURE) ;}
		else if (pid[i]==0) { 
			createProcTree(&node->children[i]);	 // recursion 
		}
		i++;
	}
	
		
	while(i>=0){
		//check=waitpid(pid[i],&status,WUNTRACED); waitpid suspends the calling process  until a specified process terminate
		
		waitpid(pid[i],&status,WUNTRACED);				   // If pid is equal to -1, the calling process waits for any child process to terminate	
		//} 
	i--;	
	}
	
	if (node->nr_children == 0){ //there are no other children put leaves to sleep
		sleep(SLEEP_PROC_SEC);
	}
	printf("%s:Bye (%ld) \n",node->name,(long) getpid());
	exit(0);
	return ;
}


int main(int argc, char *argv[])
{
        struct tree_node *root;
	pid_t PID;	
        int status;
	
	if (argc != 2) {
                fprintf(stderr, "Usage: %s <input_tree_file>\n\n", argv[0]);
                exit(1);
        }

        root = get_tree_from_file(argv[1]);
       // print_tree(root);
	PID=fork();
	
	switch(PID) {
		case 0:
			printf("Creating recursively process tree  \n");
			createProcTree(root);
       		case -1:
			perror("fork");
			exit(EXIT_FAILURE);
		default:
			printf("parent process : %d \n",getpid());
			break;
	}
	sleep(SLEEP_TREE_SEC);
	show_pstree(PID);	
	PID=wait(&status); 
	explain_wait_status(PID,status);
	return 0;
}

```

### Eρωτήσεις 

### 1)


\newpage

## Ζήτημα 1.3 Αποστολή και Χειρισμός Σημάτων {#label3}

### aks2-signals.c

```c
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "tree.h"
#include "proc-common.h"

void fork_procs(struct tree_node *root)
{
	int i=0,status;
	pid_t *pid;

	/*
	 * Start
	 */
	printf("PID = %ld, name %s, starting...\n",(long)getpid(), root->name);
	change_pname(root->name);

	pid=malloc(sizeof(pid_t)*root->nr_children);
	if (pid==NULL) { exit(3);} 	// fail to allocate memory  
	
	//8ewroume oti ka8e process dhmiourgei ta subprocesses xwris omws na perimenei ksexwrista gia ka8e ena na termatisei ,alla perimenei na termatisoun ola .
	
	while(i<root->nr_children){
		pid[i]=fork();	
		if (pid[i]<0) {
			 perror("fork");
			 exit(EXIT_FAILURE); 
		}
		if (pid[i]==0){ //create processes with BFS traversal
			 	//child 
			fork_procs(root-> children + i); // children[i] doesn run for some reason
		}
		i++;
	}
	/* This function makes sure a number of children processes
	 * have raised SIGSTOP, by using waitpid() with the WUNTRACED flag.
 	* This will NOT work if children use pause() to wait for SIGCONT.
	*/
	
	if (root->nr_children !=0){
		wait_for_ready_children(root->nr_children);
	}
	

	raise(SIGSTOP); //put current process to pause by sending SIGSTOP to the pid
	//RAISE_LABEL:
		
	printf("PID = %ld, name = %s is awake\n",(long)getpid(), root->name);

	for(i=0;i<root->nr_children;i++){
		kill(pid[i],SIGCONT); //continue stopped process (child) ( jmp  RAISE_LABEL )
		pid[i]=wait(&status); //  suspends execution until one of its children change state 
		explain_wait_status(pid[i],status);
	}	
	free(pid);
	exit(0);
}

/*
 * The initial process forks the root of the process tree,
 * waits for the process tree to be completely created,
 * then takes a photo of it using show_pstree().
 *
 * How to wait for the process tree to be ready?
 * In ask2-{fork, tree}:
 *      wait for a few seconds, hope for the best.
 * In ask2-signals:
 *      use wait_for_ready_children() to wait until
 *      the first process raises SIGSTOP.
 */

int main(int argc, char *argv[])
{
	pid_t pid;
	int status;
	struct tree_node *root;

	if (argc < 2){
		fprintf(stderr, "Usage: %s <tree_file>\n", argv[0]);
		exit(1);
	}

	/* Read tree into memory */
	root = get_tree_from_file(argv[1]);

	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs(root);
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	wait_for_ready_children(1);

	/* for ask2-{fork, tree} */
	/* sleep(SLEEP_TREE_SEC); */

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* continue execution of father process */
	kill(pid, SIGCONT);

	/* Wait for the root of the process tree to terminate */
	wait(&status);
	explain_wait_status(pid, status);

	return 0;
}

```

### Eρωτήσεις 

### 1)

### 2)



\newpage


## Ζήτημα 1.4 Αποστολή και Χειρισμός Σημάτων {#label4}

### aks2-pipes.c
```c
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include "tree.h"
#include "proc-common.h"
/*
pipe()  creates a pipe, a unidirectional data channel that can be used for interprocess communication.  The                                                                 
       array pipefd is used to return two file descriptors referring to the ends of the pipe.  pipefd[0] refers to                                                                 
       the read end of the pipe.  pipefd[1] refers to the write end of the pipe.  Data written to the write end of                                                                 
       the pipe is buffered by the kernel until it is read from the read end of the pipe
*/






void fork_procs(struct tree_node *root,int pfd )
{
	int i,arr[2];
	pid_t *pid;
	
	/*
	 * Start
	 */
	printf("PID = %ld, name %s, starting...\n",(long)getpid(), root->name);
	change_pname(root->name);

	pid=malloc(sizeof(pid_t)*root->nr_children);
	if (pid==NULL) {
			perror("malloc");	
			 exit(3);
	} 	// fail to allocate memory  
	
	if (pipe(arr)<0){
			perror("pipe()");
			exit(3);
	}
	
	//8ewroume oti ka8e process dhmiourgei ta subprocesses xwris omws na perimenei ksexwrista gia ka8e ena na termatisei ,alla perimenei na termatisoun ola .
	i=0;
	while(i< root->nr_children){
		pid[i]=fork();	
		if (pid[i]<0) {
			 perror("fork");
			 exit(EXIT_FAILURE); 
		}
		if (pid[i]==0){ //create processes with BFS traversal
			 	//child 
			fork_procs(root-> children + i,arr[1]); // arr[1] we need only to write
			exit(3);
		}

		i++;
	}
	/* This function makes sure a number of children processes
	 * have raised SIGSTOP, by using waitpid() with the WUNTRACED flag.
 	* This will NOT work if children use pause() to wait for SIGCONT.
	*/

	wait_for_ready_children(root->nr_children);
	

		
	// RAISE_LABEL:
	raise(SIGSTOP); //put current process to pause by sending SIGSTOP to the pid
	
	printf("PID = %ld, name = %s is awake\n",(long)getpid(), root->name);
	int status,comp;
	/* 


	* Κάθε μη τερματικός κόμβος¹ έχει ακριβώς δύο παιδιά, αναπαριστά τελεστή και το όνομά του είναι "+" ή "*".
	*  Το όνομα κάθε τερματικού κόμβου είναι ακέραιος αριθμός
	* 	
		,[5]
	    ,[+]	
	[*]    `[6] 
   	  `-10
	
	*/	
	//termatikos komvos
	if (root->nr_children == 0){ 
		comp=atoi(root->name); // atoi (convert character to integer)	
		// write to pipe
		if (write(pfd,&comp,sizeof(comp))!=sizeof(comp)){
			perror("write");	
			exit(3);
		} 
	}
	else {
		if (strcmp(root->name,"+")==0){
		comp=0;
		for(i=0;i< root->nr_children;i++){
			kill(pid[i],SIGCONT); //continue stopped process (child) ( jmp  RAISE_LABEL )
			int tmp;
			if (read(arr[0],&tmp,sizeof(tmp)) !=sizeof(tmp)){
				perror("parent failed to write in pipe");
				exit(3);
			}
			comp=comp+tmp;
			wait(&status); //  suspends execution until one of its children of process terminates.
			explain_wait_status(pid[i],status);
		}
	     }
	     else  if (strcmp(root->name,"*")==0){
                comp=1;
                for(i=0;i<root->nr_children;i++){
                        kill(pid[i],SIGCONT); //continue stopped process (child) ( jmp  RAISE_LABEL )
                        int tmp;
                        if (read(arr[0],&tmp,sizeof(tmp))!=sizeof(tmp)){
                                perror("parent failed to write in pipe");
                                exit(3);
                        }
                        comp=comp*tmp;
                        pid[i]=wait(&status); //  suspends execution until one of its children of process terminates.
                        explain_wait_status(pid[i],status);
                }
             }
	    if (write(pfd,&comp,sizeof(comp)) !=sizeof(comp)); { 
			perror("parent:write");  
			exit(3) ;
	    }
           }
      
	free(pid);
	exit(0);
}



int main(int argc, char *argv[])
{
	pid_t pid;
	int pi[2];  //read from pipe[0] 
	int status,comp; //buffer for read 
	struct tree_node *root;

	if (argc < 2){
		fprintf(stderr, "Usage: %s <tree_file>\n", argv[0]);
		exit(1);
	}

	/* Read tree into memory */
	root = get_tree_from_file(argv[1]);
	
	printf("parent creating the pipe \n");

	if ( pipe(pi)<0) {
		perror("main:pipe");
		exit(3);
	}
	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs(root,pi[1]);  // we write in pipe[1] 
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	wait_for_ready_children(1);
	
	
	/* for ask2-{fork, tree} */
	/* sleep(SLEEP_TREE_SEC); */

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* continue execution of father process */
	kill(pid, SIGCONT);
	/*Read result from pipe  */
	
	if (read(pi[0],&comp,sizeof(comp)) != sizeof(comp)){
		perror("parent: write to pipe");
		exit(0);
	}
	
	/* Wait for the root of the process tree to terminate */
	wait(&status);
	explain_wait_status(pid, status);
	printf("Final result of computation tree is : %d \n",comp);
	
	return 0;
}
```

\newpage
### Eρωτήσεις

### 1)


### 2)







