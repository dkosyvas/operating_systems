#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "tree.h"
#include "proc-common.h"

#define SLEEP_PROC_SEC 6
#define SLEEP_TREE_SEC 2 


void createProcTree( struct tree_node *node)
{
	int i=0,status;
	pid_t *pid;
	pid = (pid_t *) malloc (sizeof(pid_t) * node->nr_children );
	printf("%s: hi my pid is %ld \n",node->name, (long) getpid());
	change_pname(node->name); //name change
	
	while(i < node->nr_children ) {
		pid[i]=fork();
		if ( pid[i] < 0) { exit(EXIT_FAILURE) ;}
		else if (pid[i]==0) { 
			createProcTree(&node->children[i]);	 // recursion 
		}
		i++;
	}
	
		
	while(i>=0){
		//check=waitpid(pid[i],&status,WUNTRACED); waitpid suspends the calling process  until a specified process terminate
		
		waitpid(pid[i],&status,WUNTRACED);				   // If pid is equal to -1, the calling process waits for any child process to terminate	
		//} 
	i--;	
	}
	
	if (node->nr_children == 0){ //there are no other children put leaves to sleep
		sleep(SLEEP_PROC_SEC);
	}
	printf("%s:Bye (%ld) \n",node->name,(long) getpid());
	exit(0);
	return ;
}


int main(int argc, char *argv[])
{
        struct tree_node *root;
	pid_t PID;	
        int status;
	
	if (argc != 2) {
                fprintf(stderr, "Usage: %s <input_tree_file>\n\n", argv[0]);
                exit(1);
        }

        root = get_tree_from_file(argv[1]);
       // print_tree(root);
	PID=fork();
	
	switch(PID) {
		case 0:
			printf("Creating recursively process tree  \n");
			createProcTree(root);
       		case -1:
			perror("fork");
			exit(EXIT_FAILURE);
		default:
			printf("parent process : %d \n",getpid());
			break;
	}
	sleep(SLEEP_TREE_SEC);
	show_pstree(PID);	
	PID=wait(&status); 
	explain_wait_status(PID,status);
	return 0;
}
 

