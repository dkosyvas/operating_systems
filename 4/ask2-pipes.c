#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include "tree.h"
#include "proc-common.h"
/*
pipe()  creates a pipe, a unidirectional data channel that can be used for interprocess communication.  The                                                                 
       array pipefd is used to return two file descriptors referring to the ends of the pipe.  pipefd[0] refers to                                                                 
       the read end of the pipe.  pipefd[1] refers to the write end of the pipe.  Data written to the write end of                                                                 
       the pipe is buffered by the kernel until it is read from the read end of the pipe
*/






void fork_procs(struct tree_node *root,int pfd )
{
	int i,arr[2];
	pid_t *pid;
	
	/*
	 * Start
	 */
	printf("PID = %ld, name %s, starting...\n",(long)getpid(), root->name);
	change_pname(root->name);

	pid=malloc(sizeof(pid_t)*root->nr_children);
	if (pid==NULL) {
			perror("malloc");	
			 exit(3);
	} 	// fail to allocate memory  
	
	if (pipe(arr)<0){
			perror("pipe()");
			exit(3);
	}
	
	//8ewroume oti ka8e process dhmiourgei ta subprocesses xwris omws na perimenei ksexwrista gia ka8e ena na termatisei ,alla perimenei na termatisoun ola .
	i=0;
	while(i< root->nr_children){
		pid[i]=fork();	
		if (pid[i]<0) {
			 perror("fork");
			 exit(EXIT_FAILURE); 
		}
		if (pid[i]==0){ //create processes with BFS traversal
			 	//child 
			fork_procs(root-> children + i,arr[1]); // arr[1] we need only to write
			exit(3);
		}

		i++;
	}
	/* This function makes sure a number of children processes
	 * have raised SIGSTOP, by using waitpid() with the WUNTRACED flag.
 	* This will NOT work if children use pause() to wait for SIGCONT.
	*/

	wait_for_ready_children(root->nr_children);
	

		
	// RAISE_LABEL:
	raise(SIGSTOP); //put current process to pause by sending SIGSTOP to the pid
	
	printf("PID = %ld, name = %s is awake\n",(long)getpid(), root->name);
	int status,comp;
	/* 


	* Κάθε μη τερματικός κόμβος¹ έχει ακριβώς δύο παιδιά, αναπαριστά τελεστή και το όνομά του είναι "+" ή "*".
	*  Το όνομα κάθε τερματικού κόμβου είναι ακέραιος αριθμός
	* 	
		,[5]
	    ,[+]	
	[*]    `[6] 
   	  `-10
	
	*/	
	//termatikos komvos
	if (root->nr_children == 0){ 
		comp=atoi(root->name); // atoi (convert character to integer)	
		// write to pipe
		if (write(pfd,&comp,sizeof(comp))!=sizeof(comp)){
			perror("write");	
			exit(3);
		} 
	}
	else {
		if (strcmp(root->name,"+")==0){
		comp=0;
		for(i=0;i< root->nr_children;i++){
			kill(pid[i],SIGCONT); //continue stopped process (child) ( jmp  RAISE_LABEL )
			int tmp;
			if (read(arr[0],&tmp,sizeof(tmp)) !=sizeof(tmp)){
				perror("parent failed to write in pipe");
				exit(3);
			}
			comp=comp+tmp;
			wait(&status); //  suspends execution until one of its children of process terminates.
			explain_wait_status(pid[i],status);
		}
	     }
	     else  if (strcmp(root->name,"*")==0){
                comp=1;
                for(i=0;i<root->nr_children;i++){
                        kill(pid[i],SIGCONT); //continue stopped process (child) ( jmp  RAISE_LABEL )
                        int tmp;
                        if (read(arr[0],&tmp,sizeof(tmp))!=sizeof(tmp)){
                                perror("parent failed to write in pipe");
                                exit(3);
                        }
                        comp=comp*tmp;
                        pid[i]=wait(&status); //  suspends execution until one of its children of process terminates.
                        explain_wait_status(pid[i],status);
                }
             }
	    if (write(pfd,&comp,sizeof(comp)) !=sizeof(comp)); { 
			perror("parent:write");  
			exit(3) ;
	    }
           }
      
	free(pid);
	exit(0);
}



int main(int argc, char *argv[])
{
	pid_t pid;
	int pi[2];  //read from pipe[0] 
	int status,comp; //buffer for read 
	struct tree_node *root;

	if (argc < 2){
		fprintf(stderr, "Usage: %s <tree_file>\n", argv[0]);
		exit(1);
	}

	/* Read tree into memory */
	root = get_tree_from_file(argv[1]);
	
	printf("parent creating the pipe \n");

	if ( pipe(pi)<0) {
		perror("main:pipe");
		exit(3);
	}
	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs(root,pi[1]);  // we write in pipe[1] 
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	wait_for_ready_children(1);
	
	
	/* for ask2-{fork, tree} */
	/* sleep(SLEEP_TREE_SEC); */

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* continue execution of father process */
	kill(pid, SIGCONT);
	/*Read result from pipe  */
	
	if (read(pi[0],&comp,sizeof(comp)) != sizeof(comp)){
		perror("parent: write to pipe");
		exit(0);
	}
	
	/* Wait for the root of the process tree to terminate */
	wait(&status);
	explain_wait_status(pid, status);
	printf("Final result of computation tree is : %d \n",comp);
	
	return 0;
}
